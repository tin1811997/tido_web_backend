import gulp from 'gulp'
import path from 'path'
import * as $ from './src/core/core-build.js'

gulp.task('clean', $.clean())
gulp.task('lint', $.lint())
gulp.task('flow', $.flow())
gulp.task('test', $.test())
gulp.task('inspect', $.inspect())
gulp.task('cover', $.cover())
gulp.task('debug', ['build'], $.debug())
gulp.task('watch', ['build'], $.watch(['build']))
gulp.task('default', ['watch'], $.nodemon())
gulp.task('pm2', ['watch'], $.pm2({name: 'api'}))
gulp.task('copy', $.copy(['./src/**/*.html', './src/**/*.txt', './**/npm-postinstall.sh']))
gulp.task('copy-bin', $.copy(['./src/bin/**/*'], path.join($.Paths.BUILD, 'bin')))
gulp.task('build', ['copy', 'copy-bin'], $.build())
