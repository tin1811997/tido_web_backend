import 'source-map-support/register'
import 'babel-polyfill'
import App from './app'

const app = new App()

if (!module.parent) {

  // Enable Keymetrics monitoring: https://www.npmjs.com/package/pmx
  require('pmx').init({
    errors: true,
    custom_probes: true,
    network: false,
    ports: true
  })

  app.start()
}
