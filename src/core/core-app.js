import '../api/modules/user/models/user.model'
import crypto from 'crypto'
import BPromise from 'bluebird'
import config from '../config/config'
import UserQuery from '../api/modules/user/models/user.model'
import Logger from '../logger/logger'

const hashAsync = BPromise.promisify(require('bcrypt').hash)
const compareHashAsync = BPromise.promisify(require('bcrypt').compare)
const jwtSignAsync = BPromise.promisify(require('jsonwebtoken').sign)
const jwtVerifyAsync = BPromise.promisify(require('jsonwebtoken').verify)

const SALT_ROUNDS = 12
const JWT_KEY = config.auth.jwtKey
const ENCRYPTION_KEY = config.auth.encryptKey
const CHARACTER_ID = config.characterRandom.aesIvKey

export default class CoreApp {
  constructor() {
    this._logger = new Logger()
  }

  async validateBasicAuth(authorization) {
    let _this = this
    return new BPromise (async resolve => {
      if (!authorization || authorization.indexOf('TiDoAuthentication ') === -1) {
        return resolve(null)
      }

      const base64Credentials =  authorization.split(' ')[1]
      const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii')
      const [basicUser, basicPass] = credentials.split(':')
      let userQuery = new UserQuery()
      const user = await userQuery.filter(true,{username: basicUser})
      if (!user || !user.hash|| !basicPass) {
        return resolve(null)
      }
      else {
        try {
          const basicAuth = _this.decryptContent(basicPass)
          const authHash = _this.decryptContent(user.hash)
          if (!await _this.validateHash(basicAuth, authHash)) {
            return resolve(null)
          }

          return resolve(user)
        }
        catch (error) {
          _this._logger.errorLog(error, _this.constructor.name,'validateBasicAuth')
          return resolve(null)
        }
      }
    })
  }

  /**
   * JWT
   * */
  async createToken(data, expiration) {
    try {
      const payload = expiration ? {exp: expiration, data: data} : data
      return await jwtSignAsync(payload, JWT_KEY)
    }
    catch (error) {
      this._logger.errorLog(error, this.constructor.name,'createToken')
      return null
    }
  }

  async verifyToken(token) {
    try {
      return await jwtVerifyAsync(token, JWT_KEY)
    }
    catch (error) {
      this._logger.errorLog(error, this.constructor.name,'verifyToken')
      return null
    }
  }

  /**
   * Hash
   * */
  async createHash(context) {
    try {
      return await hashAsync(context, SALT_ROUNDS)
    }
    catch (error) {
      this._logger.errorLog(error, this.constructor.name,'createHash')
      return null
    }
  }

  async validateHash(src, des) {
    try {
      return await compareHashAsync(src, des)
    }
    catch (error) {
      this._logger.errorLog(error, this.constructor.name,'validateHash')
      return null
    }
  }

  /**
   * AesCrypt
   * */
  encryptContent(context) {
    try {
      let iv = ''
      for (let i = 0; i < 16; i++){
        iv += CHARACTER_ID.charAt(Math.floor(Math.random() * CHARACTER_ID.length))
      }

      let cipher = crypto.createCipheriv('aes-128-cbc', ENCRYPTION_KEY, iv)
      let base64_text = Buffer.from(context).toString('base64')
      let result = cipher.update(base64_text, 'utf8', 'base64')
      result += cipher.final('base64')
      return iv+result
    }
    catch (error) {
      this._logger.errorLog(error, this.constructor.name,'encryptContent')
      return null
    }
  }

  decryptContent(context) {
    try {
      const iv = context.slice(0, 16)
      const data = context.slice(16)
      const decipher = crypto.createDecipheriv('aes-128-cbc', ENCRYPTION_KEY,
        Buffer.from(iv, 'base64').toString('base64'))
      let result = decipher.update(data, 'base64', 'utf8')
      result += decipher.final('utf8')
      return Buffer.from(result, 'base64').toString('ascii')
    }
    catch (error) {
      this._logger.errorLog(error, this.constructor.name,'decryptContent')
      return null
    }
  }
}
