
export default class CoreController {
  constructor(response) {
    this._res = response
  }

  sendResponse(message, statusCode, data = {}) {
    if (this._res) {
      if (typeof this._res === "function") {
        this._res(statusCode, message , data)
      }
      else {
        this._res.status(statusCode || 401).send({
          error: true,
          message: message
        })
      }
    }

    throw new Error('VALID')
  }
}
