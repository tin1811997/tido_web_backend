import CoreApp from "./core-app"
import {USER_EVENTS} from "../hub/constants"

export function simpleObj (obj) {
  let cache = []
  let text = JSON.stringify(obj, function (key, value) {
    if (typeof value === 'object' && value !== null) {
      if (cache.indexOf(value) !== -1) {
        try {
          return JSON.parse(JSON.stringify(value))
        }
        catch (error) {
          return
        }
      }
      cache.push(value)
    }

    return value
  })

  return JSON.parse(text)
}

export function cloneObject (obj) {
  let copy
  if (obj ===  null|| typeof obj !== 'object') {
    return obj
  }

  // Handle Date
  if (obj instanceof Date) {
    copy = new Date()
    copy.setTime(obj.getTime())
    return copy
  }

  // Handle Array
  if (obj instanceof Array) {
    copy = []
    for (let i = 0, len = obj.length; i < len; i++) {
      copy[i] = cloneObject(obj[i])
    }

    return copy
  }

  // Handle Object
  if (obj instanceof Object) {
    copy = {}
    for (let attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = cloneObject(obj[attr])
    }
    return copy
  }
}

export function wrapAsync(bindObject, ...functions) {
  return functions.map(fn => {
    fn = fn.bind(bindObject)
    return function (req, res, next) {
      fn(req, res, next).catch(next)
    }
  })
}

export function Timer(fn, t) {
  var timerObj = setInterval(fn, t)

  this.stop = function() {
    if (timerObj) {
      clearInterval(timerObj)
      timerObj = null
    }

    return this
  }

  // start timer using current settings (if it's not already running)
  this.start = function() {
    if (!timerObj) {
      this.stop()
      timerObj = setInterval(fn, t)
    }

    return this
  }

  // start with new interval, stop current interval
  this.reset = function(newT) {
    t = newT
    return this.stop().start()
  }
}

/**
 * Socket route wrapper
 */
// For device
export function dFlowCtrl(content, channelEven, data, ...args) {
  const socket = content._socket
  const object = content._objCtrl
  const logger = content._logger
  const argsLength = args.length
  const coreApp = new CoreApp()
  let reqObj = {}
  if (typeof (data) === 'object') {
    reqObj['payloads'] = data
  }
  else {
    reqObj['payloads'] = JSON.parse(coreApp.decryptContent(data))
  }

  let resObj = function(statusCode, message = '', payloads = {}) {
    let dataSend
    if (statusCode !== 200) {
      dataSend = {error: true, message: message}
    }
    else {
      dataSend = payloads
    }

    socket.emit(`${channelEven}_RES`, coreApp.encryptContent(JSON.stringify(dataSend)))
    logger.infoLog(`SOCKET - ${socket.id} - ${channelEven} ${statusCode}`)
    throw new Error('VALID')
  }

  let arrFunc = args.map((func, index)=> {
    func = func.bind(object)
    return index + 1 === argsLength ? () => {
      func(reqObj, resObj.bind(object)).catch((error) => {
        if (error.message !== 'VALID') {
          logger.errorLog(error)
        }
      })
    } : (next) => {
      func(reqObj, resObj.bind(object), next).catch((error) => {
        if (error.message !== 'VALID' && error.message !== "Cannot read property 'catch' of undefined") {
          logger.errorLog(error)
        }
      })
    }
  })

  const final = arrFunc.pop()
  _series(arrFunc, final)
}

// For user
export function uFlowCtrl(content, channelEven, data, ...args) {
  const socket = content._socket
  const object = content._objCtrl
  const logger = content._logger

  const passPayload = USER_EVENTS.dTimer.deleteAlert
  const passPayloadRoute = USER_EVENTS.init
  const argsLength = args.length
  if (argsLength && data && data.headers && Object.keys(data.headers).length !== 0 &&
    (passPayloadRoute || (data.routes && Object.keys(data.routes).length !== 0 &&
      data.payloads && (passPayload || Object.keys(data.payloads).length !== 0)))) {
    let reqObj = {}
    reqObj['socketId'] = socket.id
    reqObj['headers'] = data.headers
    reqObj['routes'] = data.routes
    reqObj['payloads'] = data.payloads
    let resObj = function(statusCode, message = '', payloads = {}) {
      socket.emit(`${channelEven}_RES`, {error: statusCode !== 200, message: message, payloads: payloads})
      logger.infoLog(`SOCKET - ${socket.id} - User : ${channelEven} ${statusCode}`)
      if (message === 'Unauthorized') {
        socket.disconnect(true)
      }

      throw new Error('VALID')
    }

    let arrFunc = args.map((func, index)=> {
      func = func.bind(object)
      return index + 1 === argsLength ? () => {
        func(reqObj, resObj.bind(object)).catch((error) => {
          if (error.message !== 'VALID') {
            logger.errorLog(error)
          }
        })
      } : (next) => {
        func(reqObj, resObj.bind(object), next).catch((error) => {
          if (error.message !== 'VALID' && error.message !== "Cannot read property 'catch' of undefined") {
            logger.errorLog(error)
          }
        })
      }
    })

    const final = arrFunc.pop()
    _series(arrFunc, final)
  }
  else {
    socket.emit(`${channelEven}_RES`, {error: true, message: 'Invalid data', payloads: {}})
    socket.disconnect(true)
  }
}

function _series(callbacks, last) {
  let _req = {}, _res = {}
  next()
  function next() {
    let callback = callbacks.shift()
    if(callback) {
      callback((req, res) => {
        _req = req
        _res = res
        next()
      })
    }
    else {
      last(_req, _res)
    }
  }
}
