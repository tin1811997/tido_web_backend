import {cloneObject, simpleObj} from './core-common'
import Logger from '../logger/logger'
import {FieldRemove} from '../api/modules/constants'
import CoreController from "./core-controller";

export default class CoreAgent extends CoreController {
  constructor(model, response) {
    super(response)
    this._model = this._modelOrigin = model
    this._logger = new Logger()
  }

  createdModel(data) {
    this._model = this._modelOrigin
    let _this = this
    return new Promise(async resolve => {
      let _model = new this._model(data)
      _model.save(function (err, doc) {
        if (err) {
          _this._logger.errorLog(err, _this.constructor.name, 'createdModel', data)
          return resolve(null)
        }
        else if (doc && doc._id) {
          doc = cloneObject(simpleObj(doc))
          FieldRemove.map(fieldInvalid => {
            if (doc.hasOwnProperty(fieldInvalid)) {
              delete doc[fieldInvalid]
            }
          })

          resolve(doc)
        }
        else {
          _this._logger.errorLog(null, _this.constructor.name, 'createdModel', data)
          resolve()
        }
      })
    })
  }

  updatedModel(data, filter, option) {
    let _this = this
    _this._model = _this._modelOrigin
    filter = filter ? filter : {id: data.id}
    option = option && typeof option === "object" && Object.keys(option).length ?
      Object.assign({new: true}, option) : {new: true}
    return new Promise(async resolve => {
      _this._model.findOneAndUpdate(filter, data, option, (err, doc) => {
        if (err) {
          _this._logger.errorLog(err, _this.constructor.name, 'updatedModel', data)
          resolve()
        }
        if (doc && doc._id) {
          doc = cloneObject(simpleObj(doc))
          FieldRemove.map(fieldInvalid => {
            if (doc.hasOwnProperty(fieldInvalid)) {
              delete doc[fieldInvalid]
            }
          })

          resolve(doc)
        } else {
          _this._logger.errorLog(null, _this.constructor.name, 'updatedModel', data)
          resolve()
        }
      })
    })
  }

  deletedModel(data, filter) {
    let _this = this
    _this._model = _this._modelOrigin
    filter = filter ? filter : {id: data.id}
    return new Promise(async resolve => {
      _this._model.findOneAndRemove(filter, (err, doc) => {
        if (err) {
          _this._logger.errorLog(err, _this.constructor.name, 'deletedModel', data)
        }

        doc = cloneObject(simpleObj(doc))
        FieldRemove.map(fieldInvalid => {
          if (doc.hasOwnProperty(fieldInvalid)) {
            delete doc[fieldInvalid]
          }
        })

        resolve(doc)
      })
    })
  }

  updatedMultiModel(data, filter, option) {
    let _this = this
    _this._model = _this._modelOrigin
    option = option && typeof option === "object" && Object.keys(option).length ?
      Object.assign({multi: true, new: true}, option) : {multi: true, new: true}
    return new Promise(async resolve => {
      _this._model.update(filter, {$set: data}, option, (err, count) => {
        if (err) {
          _this._logger.errorLog(err, _this.constructor.name, 'updatedMultiModel', data)
        }

        resolve(count)
      })
    })
  }
}
