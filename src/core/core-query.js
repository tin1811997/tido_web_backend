import {cloneObject, simpleObj} from './core-common'
import {FieldRemove} from '../api/modules/constants'
import Logger from '../logger/logger'

export default class CoreQuery {
  constructor(collection) {
    this._collection = collection
    this._collectionOrigin = collection
    this._criteria = {}
    this._group = {}
    this._match = {}
    this._project = {}
    this._sort = {}
    this._limit = -1
    this._skip = -1

    this._logger = new Logger()
  }

  _checkAggregate() {
    let pass = false
    if (Object.keys(this._group).length || Object.keys(this._match).length) {
      pass = true
    }
    return pass
  }

  _setMatch() {
    if (this._match && Object.keys(this._match).length) {
      this._collection = this._collection.match(this._match)
    }
  }

  _setGroup() {
    if (this._group && Object.keys(this._group).length) {
      this._collection = this._collection.group(this._group)
    }
  }

  _setProject() {
    if (this._project && Object.keys(this._project).length) {
      this._collection = this._collection.project(this._project)
    }
  }

  _setSkip() {
    if (this._skip && this._skip >= 0) {
      this._collection = this._collection.skip(this._skip)
    }
  }

  _setLimit() {
    if (this._limit && this._limit >= 0) {
      this._collection = this._collection.limit(this._limit)
    }
  }

  _setSort() {
    if (this._sort && Object.keys(this._sort).length) {
      this._collection = this._collection.sort(this._sort)
    }
  }

  _setCursor() {
    this._collection = this._collection.cursor()
    if (this._checkAggregate()) {
      this._collection = this._collection.exec()
    }
  }

  _setFunction(only) {
    this._collection = this._collectionOrigin
    if (this._checkAggregate()) {
      this._collection = this._collection.aggregate()
      this._setMatch()
      this._setGroup()
    }
    else if (only) {
      this._collection = this._collection.findOne(this._criteria)
    }
    else{
      this._collection = this._collection.find(this._criteria)
    }

    this._setProject()
    this._setSkip()
    this._setLimit()
    this._setSort()
    this._setCursor()
  }

  _startQuery(onData, onEnd, onError, only) {
    let _this = this
    let data = []
    return new Promise((resolve) => {
      _this._collection.on('data', doc => {
        doc = cloneObject(simpleObj(doc))
        FieldRemove.map(fieldInvalid => {
          if (doc.hasOwnProperty(fieldInvalid)) {
            delete doc[fieldInvalid]
          }
        })

        if (onData) {
          onData(doc)
        }

        data.push(doc)
      }).on('error', (err) => {
        if (onError) {
          onError(err)
        }

        _this._logger.errorLog(err, _this.constructor.name,'_startQuery')
        resolve(null)
      }).on('end', function () {
        if (onEnd) {
          onEnd(data)
        }

        resolve(only ? data[0] : data)
      })
    })
  }

  _getCount() {
    let _this = this
    return new Promise((resolve) => {
      _this._collection.countDocuments(_this._criteria, function (err, count) {
        if (err) {
          this._logger.errorLog(err, _this.constructor.name,'_getCount')
          resolve(null)
        }
        else {
          resolve(count)
        }
      })
    })
  }

  async queryData(only = false, onData, onEnd, onError) {
    this._setFunction(only)
    return await this._startQuery(onData, onEnd, onError, only)
  }

  async countData() {
    return await this._getCount()
  }
}
