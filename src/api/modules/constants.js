export const SyncStatus = {
  Done: 1,
  Pending: 2,
  NoDeviceConnection: 3,
  NoUserConnection: 4,
  Undefined: 0
}

export const DeviceType = {
  Switch1: 'SWIT01',
  Switch2: 'SWIT02',
  Switch3: 'SWIT03',
  RollingDoor: 'ROLDOR',
  LightRGB: 'LEDCOL',
  Monitor_001: 'MON001',    // Ph Oxy Temperature Humidity Lux
  Timer_2p_5t: 'TIME25',
  Timer_2p_20t: 'TIME22',
  Timer_8p_5t: 'TIME85',
  Timer_8p_20t: 'TIME82',
}

export const FieldRemove = ['_id', '__v', 'isDelete']
