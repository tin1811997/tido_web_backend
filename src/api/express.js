import express from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import session from 'express-session'
import Mongostore from 'connect-mongo'
import compress from 'compression'
import methodOverride from 'method-override'
import cookieParser from 'cookie-parser'
import helmet from 'helmet'
import flash from 'connect-flash'
import path from 'path'
import lusca from 'lusca'
import {getGlobbedPaths} from '../core/core-kfs'
import Logger from '../logger/logger'
// import Hub from '../hub/socket.io.js'

const MongoStore = Mongostore(session)
const MONTH_MAPPING = {
  'Jan': '01',
  'Feb': '02',
  'Mar': '03',
  'Apr': '04',
  'May': '05',
  'Jun': '06',
  'Jul': '07',
  'Aug': '08',
  'Sep': '09',
  'Oct': '10',
  'Nov': '11',
  'Dec': '12'
}

export default class Api {
  constructor(dbConnection, config) {
    this._config = config
    this._app = express()
    this._initMiddleware(this._app)
    this._initHelmetHeaders(this._app)
    this._initSession(this._app, dbConnection)
    this._initRoutes(this._app)
    this._initErrorException(this._app)
    // this._app = this._configureHub(this._app)
  }

  getGlobalApp() {
    return this._app
  }

  _initMiddleware(app) {
    // Should be placed before express.static
    app.use(compress({
      filter: (req, res) => {
        return (/json|text|javascript|css|font|svg/).test(res.getHeader('Content-Type'))
      },
      level: 9
    }))

    morgan.token('date', function() {
      let p = new Date().toString().replace(/[A-Z]{3}\+/,'+').split(/ /)
      return(p[3]+'-'+MONTH_MAPPING[p[1]]+'-'+p[2]+' '+p[4])
    })
    let logger = new Logger()
    app.use(morgan(logger.getMorganFormat(), logger.getMorganStream()))
    app.use(bodyParser.urlencoded({
      extended: true
    }))
    app.use(bodyParser.json())
    app.use(methodOverride())
    app.use(cookieParser())
    app.use(flash())
  }

  _initSession(app, dbConnection) {
    // Express MongoDB session storage
    app.use(session({
      saveUninitialized: true,
      resave: true,
      secret: this._config.session.sessionSecret,
      cookie: {
        maxAge: this._config.session.sessionCookie.maxAge,
        httpOnly: this._config.session.sessionCookie.httpOnly,
        secure: this._config.session.sessionCookie.secure
      },
      name: this._config.session.sessionKey,
      store: new MongoStore({
        mongooseConnection: dbConnection,
        collection: this._config.session.sessionCollection
      })
    }))

    app.use(lusca(this._config.csrf))
  }

  _initRoutes(app) {
    const routesPath = getGlobbedPaths(this._config.files.routes)
    routesPath.forEach(function (routePath) {
      require(path.resolve(routePath))(app)
    })
  }

  //use to handler error for request API
  _initErrorException(app) {
    app.use(function(err, req, res, next) {
      if (err.message !== 'VALID') {
        const logger = new Logger()
        logger.errorLog(err.stack)
        return res.status(500).send({
          error: true,
          message: "Đã xảy ra lỗi. Vui lòng liên hện quản trị hệ thống để khắc phục."
        })
      }
    })
  }

  _initHelmetHeaders(app) {
    app.use(helmet.frameguard())
    app.use(helmet.xssFilter())
    app.use(helmet.noSniff())
    app.use(helmet.ieNoOpen())
    app.use(helmet.hsts(this._config.helmetHSTS))
    app.disable('x-powered-by')
  }

  // _configureHub(app) {
  //   let hub = new Hub(app)
  //   return hub._server
  // }
}
