import winston from 'winston'
import util from 'util'
import chalk from 'chalk'
import _ from 'lodash'
import config from '../config/config'
import 'winston-daily-rotate-file'

// list of valid formats for the logging
const VALID_FORMATS = ['combined', 'common', 'dev', 'short', 'tiny'];

const CHALK_COLOR = [
  'green',
  'yellow',
  'blue',
  'magenta',
  'cyan',
  'purple',
  'fuchsia',
  'lime',
  'navy',
  'teal',
  'yellowgreen',
  'orange'
]

export default class Logger {
  constructor(isChalk) {
    this._logger = winston.createLogger({
      level: 'debug',
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss'} ),
        winston.format.printf(info => `[${info.timestamp}] - ${info.message}`)
      ),
      defaultMeta: {service: 'user-service'},
      transports: [],
      exitOnError: false
    })

    this._chalk = isChalk ? chalk.keyword(CHALK_COLOR[Math.floor(Math.random() * CHALK_COLOR.length)]) : chalk
    this.setupTransports()
  }

  socketDebugLogSend(send) {
    this._logger.debug(this._chalk(`\t\t${chalk.redBright.bold('✗')  }\tUser ---▶`, util.format('%o', send),    '--▶  SEND   --▶ Device --▶ |'))
  }

  socketDebugLogReceive(receive) {
    this._logger.debug(this._chalk(`\t\t${chalk.greenBright.bold('✓')}\tUser ◀---`, util.format('%o', receive), '◀-- RECEIVE ◀-- Device ◀-- ▼'))
  }

  infoLog(message) {
    this._logger.info(this._chalk(message))
  }

  errorLog(error, classErr, functionErr, dataError) {
    if (error && error.stack) {
      error = error.stack
    }

    let errorDetail = {}
    if (classErr) {
      errorDetail.classError = classErr
    }

    if (functionErr) {
      errorDetail.functionError = functionErr
    }

    if (dataError) {
      errorDetail.dataError = dataError
    }

    if (errorDetail && (classErr || functionErr || dataError)) {
      this._logger.error(error, '\nDetail: ' ,util.format('%o', errorDetail), '\n')
    }
    else {
      this._logger.error(error, '\n')
    }
  }

  setupTransports() {
    this._logger.clear()
    for(let key in config.log.transports) {
      if (Object.prototype.hasOwnProperty.call(config.log.transports, key)) {
        const transport = config.log.transports[key]
        if (transport.isConsole) {
          this._logger.add(new winston.transports.Console(config.log.transports[key]))
        }
        else {
          this._logger.add(new winston.transports.DailyRotateFile(config.log.transports[key]))
        }
      }
    }
  }

  getMorganFormat() {
    let format = config.log && config.log.format ? config.log.format.toString() : 'combined'
    if (!_.includes(VALID_FORMATS, format)) {
      format = 'combined'
    }

    return format;
  }

  getMorganStream() {
    let _this = this
    return {
      stream: {
        write: message => {
          _this.infoLog(message)
        }
      }
    }
  }
}
