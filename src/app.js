import chalk from 'chalk'
import config from './config/config'
import DBConnection from './db/mongoose.js'
import Api from './api/express.js'

const SERVER_HOST = config.host
const SERVER_PORT = config.port

export default class App {
  constructor() {
  }

  async start() {
    const mongooseService = new DBConnection()
    const connection = await mongooseService._connection()
    if (connection) {
      const api = new Api(connection, config)
      const app = api.getGlobalApp()
      app.listen(SERVER_PORT, SERVER_HOST, function () {
        console.log(chalk.gray('==================================================='))
        console.log(chalk.blue    (`\t\t\t${config.app.title}`))
        console.log(chalk.gray('==================================================='))
        console.log(chalk.cyan    (`\tEnvironment:\t${config.env}`))
        console.log(chalk.green   (`\tServer:`))
        console.log(chalk.green   (`\t\t + Host  :\t${config.host}`))
        console.log(chalk.green   (`\t\t + Port  :\t${config.port}`))
        console.log(chalk.magenta (`\tDatabase:`))
        console.log(chalk.magenta (`\t\t + URI   :\t${config.db.uri}`))
        console.log(chalk.magenta (`\t\t + Port  :\t${config.db.port}`))
        console.log(chalk.magenta (`\t\t + Name  :\t${config.db.name}`))
        console.log(chalk.gray('==================================================='))

        process.on('exit', () => {
          let socketIO = global.socketIO
          if (socketIO){
            Object.keys(socketIO.sockets.connected).forEach(function(socketId) {
              socketIO.sockets.connected[socketId].disconnect(true)
            })
          }
        })
      })
    }
  }
}
