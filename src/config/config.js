import path from 'path'
import _ from 'lodash'
import chalk from 'chalk'
import glob from 'glob'
import fs from 'fs'

const NODE_ENV = process.env.NODE_ENV || 'development'
const PARENT_PATH = NODE_ENV === 'development' ? 'src' : 'build'

let assets = {
  models: path.join(process.cwd(), PARENT_PATH, '/api/modules/*/models/**/*.js'),
  routes: path.join(process.cwd(), PARENT_PATH, '/api/modules/*/routes/**/*.js'),
  sockets: path.join(process.cwd(), PARENT_PATH, '/api/modules/sockets/*.js'),
  policies: path.join(process.cwd(), PARENT_PATH, '/api/modules/*/policies/*.js'),
}

function getGlobbedPaths (globPatterns, excludes) {
  let urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i')
  let output = []

  if (_.isArray(globPatterns)) {
    globPatterns.forEach(function (globPattern) {
      output = _.union(output, getGlobbedPaths(globPattern, excludes))
    })
  }
  else if (_.isString(globPatterns)) {
    if (urlRegex.test(globPatterns)) {
      output.push(globPatterns)
    }
    else {
      let files = glob.sync(globPatterns)
      if (excludes) {
        files = files.map(function (file) {
          if (_.isArray(excludes)) {
            for (let i in excludes) {
              if (excludes.hasOwnProperty(i)) {
                file = file.replace(excludes[i], '')
              }
            }
          }
          else {
            file = file.replace(excludes, '')
          }

          return file
        })
      }

      output = _.union(output, files)
    }
  }

  return output
}

function initGlobalConfigFiles (config, assets) {
  config.files = {}
  config.files.models = getGlobbedPaths(assets.models)
  config.files.routes = getGlobbedPaths(assets.routes)
  config.files.configs = getGlobbedPaths(assets.config)
  config.files.sockets = getGlobbedPaths(assets.sockets)
  config.files.policies = getGlobbedPaths(assets.policies)
}

function validateSecureMode (config) {

  if (!config.secure || config.secure.ssl !== true) {
    return true
  }

  let privateKey = fs.existsSync(path.resolve(config.secure.privateKey))
  let certificate = fs.existsSync(path.resolve(config.secure.certificate))

  if (!privateKey || !certificate) {
    console.log(chalk.red('+ Error: Certificate file or key file is missing, falling back to non-SSL mode'))
    console.log(chalk.red('  To create them, simply run the following from your shell: sh ./scripts/generate-ssl-certs.sh'))
    console.log()
    config.secure.ssl = false
  }
}

function validateSessionSecret (config, testing) {

  if (process.env.NODE_ENV !== 'production') {
    return true
  }

  if (config.sessionSecret === 'MEAN') {
    if (!testing) {
      console.log(chalk.red('+ WARNING: It is strongly recommended that you change sessionSecret config while running in production!'))
      console.log(chalk.red('  Please add `sessionSecret: process.env.SESSION_SECRET || \'super amazing secret\'` to '))
      console.log(chalk.red('  `config/env/production.js` or `config/env/local.js`'))
      console.log()
    }
    return false
  } else {
    return true
  }
}

function validateDomainIsSet (config) {
  if (!config.domain) {
    console.log(chalk.red('+ Important warning: config.domain is empty. It should be set to the fully qualified domain of the app.'))
  }
}

function init() {

  // Get the default config
  let defaultConfig = require(path.join(process.cwd(), PARENT_PATH, 'config/env/default'))

  // Get the current config
  let environmentConfig = require(path.join(process.cwd(), PARENT_PATH, 'config/env/', process.env.NODE_ENV)) || {}

  // Merge config files
  let config = _.merge(defaultConfig, environmentConfig)

  // Extend the config object with the local-NODE_ENV.js custom/local environment.
  // This will override any settings present in the local configuration.
  config = _.merge(config, (fs.existsSync(path.join(process.cwd(), PARENT_PATH, `config/env/local-${NODE_ENV}.js`))
    && require(path.join(process.cwd(), PARENT_PATH, `config/env/local-${NODE_ENV}.js`))) || {})

  initGlobalConfigFiles(config, assets)

  validateSecureMode(config)

  validateSessionSecret(config)

  validateDomainIsSet(config)

  config.env = NODE_ENV
  config.parentPath = PARENT_PATH
  return config
}

let config = init()

export default config
