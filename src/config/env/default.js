const SIX_MONTHS = 15778476

export default {
  app: {
    title: 'TIDO',
    description: 'IoT connection',
    keywords: 'mongodb, express, node.js, mongoose, passport',
    author: 'Ha Vo Anh Nguyen'
  },
  domain: process.env.DOMAIN,
  host: process.env.HOST || '0.0.0.0',
  port: process.env.PORT || 5000,
  db: {
    uri: process.env.DB_MONGODB_URI || 'localhost',
    port: process.env.DB_MONGODB_PORT || 27017,
    name: process.env.DB_MONGODB_NAME || 'tido',
    username: process.env.DB_MONGODB_USER || '',
    password: process.env.DB_MONGODB_PASS || '',
    options: {},
    debug: process.env.DB_MONGODB_DEBUG || false
  },
  session: {
    sessionCookie: {
      maxAge: 24 * (60 * 60 * 1000),
      httpOnly: true,
      secure: false
    },
    sessionSecret: process.env.SESSION_SECRET || 'NODE_ES6',
    sessionKey: 'sessionId',
    sessionCollection: 'sessions',
  },
  socketio: {
    // 10s  ping packet
    // pingInterval: 10000,

    // 5s   pong packet
    timeout: 10000,
    serveClient: false,
    cookie: false
  },
  csrf: {
    csrf: false,
    csp: false,
    xframe: 'SAMEORIGIN',
    p3p: 'ABCDEF',
    xssProtection: true
  },
  helmetHSTS: {
    maxAge: SIX_MONTHS,
    includeSubDomains: false,
    force: true
  },
  auth: {
    jwtKey: process.env.JWT_KEY || 'HaVoAnhNguyen_01071994_HaVoAnhToan_04031996_TiDo_2019_BestOfTheWorld',
    encryptKey: process.env.ENCRYPTION_KEY || 'ABC123DEF456fed0'
  },
  characterRandom: {
    basicAuthKey: process.env.BASIC_AUTH_KEY || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
    aesIvKey: process.env.AES_IV_KEY || 'ABCDEFabcdef0123456789'
  }
}
