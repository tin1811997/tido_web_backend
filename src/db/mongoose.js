import mongoose from 'mongoose'
import BPromise from 'bluebird'
import config from '../config/config.js'
import Logger from '../logger/logger'

const DB_USERNAME = config.db.username
const DB_PASSWORD = config.db.password
const DB_URI = config.db.uri
const DB_PORT = config.db.port
const DB_NAME = config.db.name
const DB_OPTION = config.db.option
const DB_MONGO_URL = (DB_USERNAME && DB_PASSWORD) ?
    `mongodb://${DB_USERNAME}:${encodeURIComponent(DB_PASSWORD)}@${DB_URI}:${DB_PORT}/${DB_NAME}` :
    `mongodb://${DB_URI}:${DB_PORT}/${DB_NAME}`

export default class Mongoose {
    constructor() {
        thisis.logger = new Logger()
    }

    async _connection() {
        let _this = this
        mongoose.Promise = global.Promise
        mongoose.connect(DB_MONGO_URL, DB_OPTION)
        return new BPromise(resolve => {
            const connection = mongoose.connection

            connection.on('error', (err) => {
                _this.logger.errorLog(err, _this.constructor.name, '_connection')
                resolve(null)
            })

            connection.once('open', () => {
                mongoose.set('debug', DB_OPTION)
                resolve(connection)
            })
        })
    }
}
